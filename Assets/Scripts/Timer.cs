﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField][Range(60, 320)]private float CountDown;

    void Update()
    {
        this.TryGetComponent(out TMPro.TextMeshProUGUI tmp);
        tmp.text = ((int)CountDown).ToString();

        CountDown -= Time.deltaTime;

        if (CountDown <= 0.0f) Camera.main.GetComponent<GameMotor>().lose();
    }
}
