﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPControler : MonoBehaviour
{
    private float x, z;
    private float mousex, mousey;
    private float vertical;
    [SerializeField][Range(0, 10)]private int velocity;
    private float velMod;

    private Vector3 moveVelocity;
    private Vector3 moveVelocitySup;

    private Rigidbody rb;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        rb = this.transform.GetComponent<Rigidbody>();

        velMod = 1;

        Camera.main.transform.localEulerAngles = Vector3.zero;
        vertical = 0;
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        velMod = Input.GetKey(KeyCode.LeftShift) ? 2.5f : 1f;

        if (x != 0 || z != 0)
        {
            moveVelocity = transform.forward * (new Vector3(0, 0, z).sqrMagnitude) * velocity * velMod * (z > 0 ? 1 : -1);
            moveVelocitySup = transform.right * (new Vector3(x, 0, 0).sqrMagnitude) * velocity * (x > 0 ? 1 : -1);
        }
        else
        {
            moveVelocity = Vector3.zero;
        }


        mousex = Input.GetAxis("Mouse X");
        mousey = Input.GetAxis("Mouse Y");

        if(mousex != 0 || mousey != 0)
        {
            this.transform.Rotate(new Vector3(0, mousex, 0));
            vertical = Mathf.Clamp(vertical - mousey, -40f, 60f);
            Camera.main.transform.localEulerAngles = new Vector3(vertical,
                                                                 Camera.main.transform.localEulerAngles.y,
                                                                 Camera.main.transform.localEulerAngles.z);
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = moveVelocity + moveVelocitySup + new Vector3(0, rb.velocity.y, 0);
    }
}
