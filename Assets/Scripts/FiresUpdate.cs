﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiresUpdate : MonoBehaviour
{
    private int kazu;
    private int ima;

    private GameObject[] fires;
    void Start()
    {
        fires = GameObject.FindGameObjectsWithTag("Respawn");

        kazu = fires.Length / 2;
    }

    // Update is called once per frame
    void Update()
    {
        this.TryGetComponent(out TMPro.TextMeshProUGUI tmp);

        ima = 0;
        foreach (GameObject go in fires)
        {
            if (go.activeSelf)
            {
                ima++;
            }
        }

        tmp.text = ima + "/" + kazu;

        if (ima == 0) Camera.main.GetComponent<GameMotor>().win();
    }
}
