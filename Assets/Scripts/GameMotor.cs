﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMotor : MonoBehaviour
{
    private void stopTime()
    {
        Time.timeScale = 0f;
    }
    private void playTime()
    {
        Time.timeScale = 1f;
    }

    public void win()
    {
        stopTime();
        GameObject.Find("GUI/Canvas/Win").SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }
    public void lose()
    {
        stopTime();
        GameObject.Find("GUI/Canvas/Lose").SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }
    public void continuar()
    {
        GameObject.Find("GUI/Canvas/Win").SetActive(false);
        playTime();
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        playTime();
    }
    public void exit()
    {
        Application.Quit();
    }
    public void changeScene(int i)
    {
        SceneManager.LoadScene(i);
        playTime();
    }
}
