﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFire : MonoBehaviour
{
    GameObject[] hijos;
    private int cont;
    void Start()
    {
        hijos = GameObject.FindGameObjectsWithTag("Respawn");
        cont = 0;

        while(cont != hijos.Length / 2)
        {
            int i = Random.Range(0, hijos.Length - 1);
            if (hijos[i].activeSelf)
            {
                hijos[i].SetActive(false);
                cont++;
            }
        }
    }
}
