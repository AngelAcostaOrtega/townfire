﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour
{
    private Ray ray;
    private RaycastHit hit;
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);

        if( Physics.Raycast(ray, out hit) )
        {
            if ( (hit.transform.gameObject.CompareTag("Pointable") || hit.transform.gameObject.CompareTag("Respawn"))
               && hit.distance <= 2 )
            {
                if( (hit.transform.gameObject.name.Equals("Pozo") || hit.transform.gameObject.CompareTag("Respawn") || hit.transform.gameObject.name.Equals("TinyFire"))
                 && !GameObject.Find("Player/Main Camera/Cubo").activeSelf)
                {
                    GameObject.Find("GUI/Canvas/Pointer/LPress").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LMizuNai").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LNai").SetActive(true);
                }
                else if (!hit.transform.gameObject.name.Equals("Pozo")
                     && !hit.transform.gameObject.name.Equals("Cubo")
                     && !GameObject.Find("Player/Main Camera/Cubo/Mizu").activeSelf)
                {
                    GameObject.Find("GUI/Canvas/Pointer/LNai").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LPress").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LMizuNai").SetActive(true);
                }
                else
                {
                    GameObject.Find("GUI/Canvas/Pointer/LNai").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LMizuNai").SetActive(false);
                    GameObject.Find("GUI/Canvas/Pointer/LPress").SetActive(true);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        if (hit.transform.gameObject.name.Equals("Cubo"))
                        {
                            Destroy(hit.transform.gameObject, 0.1f);

                            GameObject.Find("Player/Main Camera/Cubo").SetActive(true);

                        }
                        if (hit.transform.gameObject.name.Equals("Pozo"))
                        {
                            if(!GameObject.Find("Player/Main Camera/Cubo/Mizu").activeSelf)
                            {
                                GameObject.Find("Player/Main Camera/Cubo/Mizu").SetActive(true);
                            }
                        }
                        if (hit.transform.gameObject.CompareTag("Respawn"))
                        {
                            if (GameObject.Find("Player/Main Camera/Cubo/Mizu").activeSelf)
                            {
                                GameObject.Find("Player/Main Camera/Cubo/Mizu").SetActive(false);
                                hit.transform.gameObject.SetActive(false);
                            }
                        }
                        if (hit.transform.gameObject.name.Equals("TinyFire"))
                        {
                            if (GameObject.Find("Player/Main Camera/Cubo/Mizu").activeSelf)
                            {
                                GameObject.Find("Player/Main Camera/Cubo/Mizu").SetActive(false);
                                hit.transform.gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }
            else
            {
                GameObject.Find("GUI/Canvas/Pointer/LPress").SetActive(false);
                GameObject.Find("GUI/Canvas/Pointer/LNai").SetActive(false);
                GameObject.Find("GUI/Canvas/Pointer/LMizuNai").SetActive(false);
            }
        }
    }
}
